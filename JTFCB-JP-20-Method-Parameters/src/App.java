class Robot {
	
	public void speak(String text){
		
		System.out.println(text);
		
	}
	
	public void jump (int height){
		
		System.out.println("Jumping " + height);
		
	}
	
	public void move(String direction, double distance) {
		
		System.out.println("Moving " + distance + " meters in direction " + direction);
		
	}
	
}
public class App {

	public static void main(String[] args) {

		Robot sam = new Robot();
		
		sam.speak("Hi I'm Sam");
		sam.jump(7);
		sam.move("west", 12.2);
		
		// teraz ten greeting to label i on wyjdzie w metodzie speak
		// pomimo ze wprowadzilem greeting to nie wywoluje bledu z parametrem text
		String greeting = "Hello there.";
		sam.speak(greeting);
		
		// tak samo tu nie koliduje pomimo ze wprowadzilem value
		// nie wywoluje bledu z parametrem height
		int value = 14;
		sam.jump(value);
	}

}
